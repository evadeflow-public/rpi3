## Overview

This repository contains a 'super project' for building custom Raspberry Pi 3 images using Yocto. It consists of:

1. A `build` folder containing the build configuration, and:
2. A `.mrconfig` file instructing the [`mr`][1] utility to clone any other required repositories

[1]: https://myrepos.branchable.com/

## Prerequisites

Before building, make sure you have all required packages installed. On Ubuntu, this can be accomplished using the following command:

```
$ sudo apt install build-essential chrpath diffstat gawk gcc-multilib git-core myrepos texinfo unzip wget
```

For Fedora, use:

```
$ sudo dnf install bzip2 ccache chrpath cpp diffstat diffutils gawk gcc gcc-c++ git glibc-devel gzip make myrepos patch perl perl-Data-Dumper perl-Text-ParseWords perl-Thread-Queue python socat tar texinfo unzip wget
```

## Initial Setup

To initialize your working copy, use the following commands:
```
$ git clone https://gitlab.com/evadeflow-public/rpi3.git
$ cd rpi3
$ echo "$PWD/.mrconfig" >> ~/.mrtrust
$ mr checkout
```

For a given working copy, these commands generally need to be run only *once*.

> **Note**:
>
> The `poky` repo is somewhat large, so the `mr checkout` command can take awhile, and may *appear* to hang. Just give it a few minutes to complete.

## Building

To build an image, run the following commands from within the `rpi3` folder:

```
$ source meta/poky/oe-init-build-env
$ bitbake rpi-basic-image
```
(Feel free to substitute `rpi-basic-image` with any other available image name.)

After the build completes, you can find the generated SD card image down in `build/tmp/deploy/images/raspberrypi3`:

```
$ ls -l build/tmp/deploy/images/raspberrypi3/rpi-basic-image-raspberrypi3.rpi-sdimg
lrwxrwxrwx. 2 dwolfe dwolfe 60 Jan 19 08:50 build/tmp/deploy/images/raspberrypi3/rpi-basic-image-raspberrypi3.rpi-sdimg -> rpi-basic-image-raspberrypi3-20170119130250.rootfs.rpi-sdimg
```

Copy it to an SD card using:

```
$ sudo dd if=rpi-basic-image-raspberrypi3.rpi-sdimg of=/dev/sd<X> bs=1M
```

insert into your RPi, and boot!
